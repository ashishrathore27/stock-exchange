package com.classklap.stockexchange.web;

import com.classklap.stockexchange.model.Exchange;
import com.classklap.stockexchange.service.ExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExchangeController {

    private ExchangeService exchangeService;

    @Autowired
    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @PostMapping("/buyorsell")
    public ResponseEntity buyOrSell(@RequestBody Exchange exchangeRequest) {
        return exchangeService.buyOrSell(exchangeRequest);
    }

    @PostMapping("/buy/multiple")
    public ResponseEntity buyMultiple(@RequestBody List<Exchange> exchangeRequest) {
        return exchangeService.buyOrSell(exchangeRequest);
    }


    @GetMapping("/order-book")
    public ResponseEntity buyOrSell() {
        return exchangeService.getOrders();
    }
}
