package com.classklap.stockexchange.model;

public enum ExchangeAction {
    SELL("sell"), BUY("buy");

    private String action;

    ExchangeAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
