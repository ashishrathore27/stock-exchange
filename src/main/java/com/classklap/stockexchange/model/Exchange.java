package com.classklap.stockexchange.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalTime;

@Document
public class Exchange {
    @Id
    private String orderId;
    private LocalTime time;
    private String stock;
    private ExchangeAction action;
    private Long quantity;
    private Double price;

    public Exchange() {

    }

    public Exchange(String orderId, LocalTime time, String stock, ExchangeAction action, Long quantity, Double price) {
        this.orderId = orderId;
        this.time = time;
        this.stock = stock;
        this.action = action;
        this.quantity = quantity;
        this.price = price;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public ExchangeAction getAction() {
        return action;
    }

    public void setAction(ExchangeAction action) {
        this.action = action;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
