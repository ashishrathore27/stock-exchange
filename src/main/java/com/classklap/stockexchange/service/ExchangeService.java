package com.classklap.stockexchange.service;

import com.classklap.stockexchange.common.Helper;
import com.classklap.stockexchange.model.Exchange;
import com.classklap.stockexchange.model.ExchangeResponse;
import com.classklap.stockexchange.model.QtyValue;
import com.classklap.stockexchange.repository.ExchangeReository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ExchangeService {

    private ExchangeReository exchangeReository;

    public ExchangeService() {
    }

    @Autowired
    public ExchangeService(ExchangeReository exchangeReository) {
        this.exchangeReository = exchangeReository;
    }

    public ResponseEntity buyOrSell(Exchange exchangeRequest) {
        if (!isSeller(exchangeRequest)) {
            return new ResponseEntity<>(buy(exchangeRequest), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(sell(exchangeRequest), HttpStatus.CREATED);
        }
    }

    public ResponseEntity buyMultiple(List<Exchange> exchangeRequest) {
        return new ResponseEntity<>(buy(exchangeRequest), HttpStatus.ACCEPTED);
    }

    private List<ExchangeResponse> buy(Exchange exchangeRequest) {
        List<Exchange> exchanges = exchangeReository.findAllExchangesForBuy(exchangeRequest.getStock(),
                exchangeRequest.getPrice(), String.valueOf(exchangeRequest.getAction()));
        List<Exchange> sortedExchanges = exchanges.stream().sorted(Comparator.comparing(Exchange::getPrice)
                .thenComparing(Exchange::getTime)).collect(Collectors.toList());
        return updateOrderBook(sortedExchanges, exchangeRequest);
    }

    private List<List<ExchangeResponse>> buy(List<Exchange> exchangeRequestList) {
        List<List<Exchange>> allExchange = new ArrayList<>();
        List<List<ExchangeResponse>> allExchangeResponse = new ArrayList<>();

        List<Exchange> exchangeListWithMatch = new ArrayList<>();
        List<Exchange> exchangeListWithNotMatch = new ArrayList<>();

         exchangeRequestList.forEach(exchange -> {
             exchangeRequestList.forEach(exchange1 -> {
                 if(exchange.getPrice().equals(exchange1.getPrice())){
                     exchangeListWithMatch.add(exchange);
                     exchangeListWithMatch.add(exchange1);
                 }
             });
         });

         exchangeRequestList.removeAll(exchangeListWithMatch);
         exchangeListWithNotMatch.addAll(exchangeRequestList);

        Long sumOfAllValue = exchangeListWithMatch.stream().mapToLong(exchange -> exchange.getQuantity()).sum();


        exchangeListWithMatch.stream().forEach(exchangeRequest -> {
            allExchangeResponse.add(updateOrderBook(exchangeReository.findAllExchangesForBuy(exchangeRequest.getStock(),
                    exchangeRequest.getPrice(), String.valueOf(exchangeRequest.getAction())),exchangeRequest, sumOfAllValue));
        });

        exchangeListWithNotMatch.stream().forEach(exchangeRequest -> {
            allExchangeResponse.add(updateOrderBook(exchangeReository.findAllExchangesForBuy(exchangeRequest.getStock(),
                    exchangeRequest.getPrice(), String.valueOf(exchangeRequest.getAction())),exchangeRequest));
        });
        return allExchangeResponse;
    }

    private Exchange sell(Exchange exchangeRequest) {
        return exchangeReository.save(exchangeRequest);
    }

    private List<ExchangeResponse> updateOrderBook(List<Exchange> exchanges, Exchange exchangeRequest) {
        Long qty = exchangeRequest.getQuantity();
        List<ExchangeResponse> exchangeResponses = new ArrayList<>();
        for (Exchange exchangeRecord : exchanges) {
            QtyValue qtyValue = Helper.calculateQty(exchangeRecord.getQuantity(), qty);
            qty = qtyValue.getRequestQty();
            exchangeResponses.add(new ExchangeResponse(exchangeRecord.getOrderId(), qtyValue.getBuyedItems(), exchangeRecord.getPrice(), exchangeRequest.getOrderId()));
            if (qtyValue.getRecordQty() == 0) {
                exchangeReository.deleteById(exchangeRecord.getOrderId());
            } else {
                exchangeReository.save(new Exchange(exchangeRecord.getOrderId(), exchangeRecord.getTime(),
                        exchangeRecord.getStock(), exchangeRecord.getAction(), qtyValue.getRecordQty(), exchangeRecord.getPrice()));
            }
            if (qty == 0) {
                break;
            }
        }
        return exchangeResponses;
    }

    private List<ExchangeResponse> updateOrderBook(List<Exchange> exchanges, Exchange exchangeRequest,Long sum) {
        Long qty = exchangeRequest.getQuantity() * 100 / sum;
        List<ExchangeResponse> exchangeResponses = new ArrayList<>();
        for (Exchange exchangeRecord : exchanges) {
            QtyValue qtyValue = Helper.calculateQty(exchangeRecord.getQuantity(), qty);
            qty = qtyValue.getRequestQty();
            exchangeResponses.add(new ExchangeResponse(exchangeRecord.getOrderId(), qtyValue.getBuyedItems(), exchangeRecord.getPrice(), exchangeRequest.getOrderId()));
            if (qtyValue.getRecordQty() == 0) {
                exchangeReository.deleteById(exchangeRecord.getOrderId());
            } else {
                exchangeReository.save(new Exchange(exchangeRecord.getOrderId(), exchangeRecord.getTime(),
                        exchangeRecord.getStock(), exchangeRecord.getAction(), qtyValue.getRecordQty(), exchangeRecord.getPrice()));
            }
            if (qty == 0) {
                break;
            }
        }
        return exchangeResponses;
    }

    private Boolean isSeller(Exchange exchange) {
        return exchange.getAction().getAction().equals("sell");
    }

    public ResponseEntity getOrders() {
        return new ResponseEntity<>(exchangeReository.findAll(), HttpStatus.OK);
    }

}
