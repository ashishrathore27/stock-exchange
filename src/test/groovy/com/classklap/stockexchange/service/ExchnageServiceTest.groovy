package com.classklap.stockexchange.service


import com.classklap.stockexchange.model.Exchange
import com.classklap.stockexchange.model.ExchangeAction
import com.classklap.stockexchange.repository.ExchangeReository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import java.time.LocalTime

class ExchnageServiceTest extends Specification {

    ExchangeReository mockExchangeReository = Mock(ExchangeReository.class)

    ExchangeService exchangeService

    def setup() {
        exchangeService = new ExchangeService(exchangeReository: mockExchangeReository);
    }

    def "buyOrSell() : should place the order in oder book and return response with status code 201"() {
        given:
        Exchange exchangeRequest = new Exchange("#1", LocalTime.now(), "EMX", ExchangeAction.SELL, 200, 200.45)
        when:
        ResponseEntity result = exchangeService.buyOrSell(exchangeRequest)
        assert result.statusCode == HttpStatus.CREATED
        assert result.body != null
        then:
        1 * mockExchangeReository.save(exchangeRequest) >> exchangeRequest
        0 * mockExchangeReository.findAllExchangesForBuy(exchangeRequest.stock, exchangeRequest.price, exchangeRequest.action)
    }

    def "buyOrSell() : should buy from oder book and return response with status code 202"() {
        given:
        Exchange exchangeRequest = new Exchange("#1", LocalTime.now(), "EMX", ExchangeAction.BUY, 200, 200.45)
        Exchange exchange1 = new Exchange("#1", LocalTime.now(), "EMX", ExchangeAction.SELL, 200, 100.45)
        Exchange exchange2 = new Exchange("#1", LocalTime.now(), "EMX", ExchangeAction.SELL, 200, 190.45)
        List<Exchange> exchangeList = new ArrayList<>();
        exchangeList.add(exchange1)
        exchangeList.add(exchange2)
        when:
        ResponseEntity result = exchangeService.buyOrSell(exchangeRequest)
        assert result.statusCode == HttpStatus.ACCEPTED
        assert result.body != null

        then:
        0 * mockExchangeReository.save(exchangeRequest) >> exchangeRequest
        1 * mockExchangeReository.findAllExchangesForBuy(_ as String, _ as Double, _ as String) >> exchangeList
    }
}
