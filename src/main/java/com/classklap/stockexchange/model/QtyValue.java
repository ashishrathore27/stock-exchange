package com.classklap.stockexchange.model;

public class QtyValue {
    private Long recordQty;
    private Long requestQty;
    private Long buyedItems;

    public QtyValue(Long recordQty, Long requestQty, Long buyedItems) {
        this.recordQty = recordQty;
        this.requestQty = requestQty;
        this.buyedItems = buyedItems;
    }

    public Long getBuyedItems() {
        return buyedItems;
    }

    public void setBuyedItems(Long buyedItems) {
        this.buyedItems = buyedItems;
    }

    public Long getRecordQty() {
        return recordQty;
    }

    public void setRecordQty(Long recordQty) {
        this.recordQty = recordQty;
    }

    public Long getRequestQty() {
        return requestQty;
    }

    public void setRequestQty(Long requestQty) {
        this.requestQty = requestQty;
    }
}
