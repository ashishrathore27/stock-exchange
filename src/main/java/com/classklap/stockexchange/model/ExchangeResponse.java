package com.classklap.stockexchange.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExchangeResponse {
    private String sellOrderId;
    private Long quantity;
    private Double sellPrice;
    private String buyOrderId;

    public ExchangeResponse(String sellOrderId, Long quantity, Double sellPrice, String buyOrderId) {
        this.sellOrderId = sellOrderId;
        this.quantity = quantity;
        this.sellPrice = sellPrice;
        this.buyOrderId = buyOrderId;

    }

    public String getSellOrderId() {
        return sellOrderId;
    }

    public void setSellOrderId(String sellOrderId) {
        this.sellOrderId = sellOrderId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getBuyOrderId() {
        return buyOrderId;
    }

    public void setBuyOrderId(String buyOrderId) {
        this.buyOrderId = buyOrderId;
    }


}
