package com.classklap.stockexchange.common;

import com.classklap.stockexchange.model.Exchange;
import com.classklap.stockexchange.model.ExchangeAction;
import com.classklap.stockexchange.model.QtyValue;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Component
public class Helper {

    public static Exchange createExchange(String exchangeRequest) {
        String[] items = exchangeRequest.split(" ");

        return new Exchange(items[0], LocalTime.parse(items[1]), items[2],
                ExchangeAction.valueOf(items[3]), Long.parseLong(items[4]),
                Double.parseDouble(items[0]));
    }

    public static QtyValue calculateQty(Long recordQty, Long requestQty) {
        QtyValue qtyValue = new QtyValue(0L, 0L, requestQty);
        long qty = 0;
        if (recordQty > requestQty) {
            qty = recordQty - requestQty;
            qtyValue.setRecordQty(qty);
            qtyValue.setRequestQty(0L);
            qtyValue.setBuyedItems(requestQty);
        } else if (requestQty > recordQty) {
            qty = requestQty - recordQty;
            qtyValue.setRecordQty(0L);
            qtyValue.setRequestQty(qty);
            qtyValue.setBuyedItems(recordQty);
        }
        return qtyValue;
    }

}
