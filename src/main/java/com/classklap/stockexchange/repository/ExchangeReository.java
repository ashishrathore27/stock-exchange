package com.classklap.stockexchange.repository;

import com.classklap.stockexchange.model.Exchange;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeReository extends MongoRepository<Exchange, String> {

    @Query("{'$and' : [{ 'stock' : {$eq : ?0}},{'price' : {$lte : ?1}}]}")
    List<Exchange> findAllExchangesForBuy(String stock, Double price, String action);

}
